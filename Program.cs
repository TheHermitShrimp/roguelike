﻿using Dungeon.Functionality;
using System;
using System.Text;

namespace Dungeon {

    internal class Program {

        private static void Main(string[] args) {
            //Console.OutputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.UTF8;
            ProgramState State = new ProgramState();
            State.Run();
        }
    }
}
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Dungeon.Entities.Items;
using freakcode.Cryptography;
using Newtonsoft.Json.Linq;

namespace Dungeon.Functionality {
	public class ItemEncyclopedia {
		public IItems Item { get; set; }
		public int Rarity { get; set; }
		public bool TreasureOnly { get; set; }
	}
	public class ItemFactory {
		private static List<ItemEncyclopedia> Items = null;
		private static void InitiateEncyclopedia() {
			if (Items == null) {
				string datafile = File.ReadAllText(Directory.GetCurrentDirectory() + "/Content/ItemEncyclopedia.json");
				JArray ja = JArray.Parse(datafile);
				if (ja.Count > 0) {
					Items = new List<ItemEncyclopedia>();
					foreach (JToken jt in ja) {
						ItemEncyclopedia IE = new ItemEncyclopedia();
						Type T = Type.GetType("Dungeon.Entities.Items." + jt.Value<string>("Type"));
						var item = Activator.CreateInstance(T);
						foreach (JProperty p in jt.Children<JProperty>()) {
							if (p.Name == "Rarity") {
								IE.Rarity = (int)p.Value.ToObject(typeof(int));
							} else if (p.Name == "TreasureOnly") {
								IE.TreasureOnly = (bool)p.Value.ToObject(typeof(bool));
							} else if (p.Name != "Type") {
								Type T2 = T.GetProperty(p.Name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly).PropertyType;
								if (T2 == typeof(Element)) {
									Type e = typeof(Element);
									T.GetProperty(p.Name).SetValue(item, e.GetField((string)p.Value.ToObject(typeof(String))).GetValue(null));
								} else {
									T.GetProperty(p.Name).SetValue(item, p.Value.ToObject(T2));
								}
							}
						}
						IE.Item = (IItems)item;
						Items.Add(IE);
					}
				}
			}
		}

		public static List<ItemDrop> GenerateItems(Map map) {
			InitiateEncyclopedia();
			List<ItemDrop> Items = new List<ItemDrop>();
            CryptoRandom R = new CryptoRandom();

			foreach (Room room in map.Rooms) {
                int ItemRoll = R.Next(0,100);
                if (ItemRoll > 97) {
                    ItemRoll = 3;
                } else if (ItemRoll > 90) {
                    ItemRoll = 2;
                } else if (ItemRoll > 70) {
                    ItemRoll = 1;
                } else {
                    ItemRoll = 0;
                }
                ItemRoll += room.IsTreasureRoom ? 1 : 0;
                for (int i = 0; i < ItemRoll; i++) {
                    int looper = 0;
                    while (true) {
                        Point p = room.getRandomPoint();
                        if (map.Entities.FirstOrDefault(e => e.x == p.x && e.y == p.y) == null) {
                            Items.Add(new ItemDrop(GachaRoll(room.IsTreasureRoom), p));
                            break;
                        }
                        looper ++;
                        if (looper > 5) {
                            break;
                        }
                    }
                }
			}

			return Items;
		}

		private static IItems GachaRoll(bool includeTreasures) {
			CryptoRandom R = new CryptoRandom();
			return Items.Where(x => includeTreasures || !x.TreasureOnly).OrderByDescending(x => R.Next(0, x.Rarity + (x.TreasureOnly == includeTreasures ? 50 : -50))).First().Item;
		}
	}
}
﻿using Dungeon.Entities;
using freakcode.Cryptography;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Dungeon.Functionality {

    public class Point : IEquatable<Point> {
        public int x {get; set;}
        public int y  {get; set;}

        public Point() {}
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

		public int CompareTo(object obj) {
			throw new NotImplementedException();
		}

		public bool Equals(Point other) {
			return this.x == other.x && this.y == other.y;
		}
	}

    public class Room {
        public int x1 { get; set; }
        public int y1 { get; set; }
        public int x2 { get; set; }
        public int y2 { get; set; }

        public bool WasOrphaned { get; set; }
        public bool IsStart { get; set; }
        public bool IsEnd { get; set; }
        public bool IsTreasureRoom = false;

        public Room(int _x1, int _y1, int _x2, int _y2) {
            this.x1 = _x1;
            this.y1 = _y1;
            this.x2 = _x2;
            this.y2 = _y2;
        }

        public bool InRoom(int x, int y) {
            return x >= x1 && x <= x2 && y >= y1 && y <= y2;
        }

        public Point getRandomPoint() {
            CryptoRandom R = new CryptoRandom();
            int x = R.Next(x1, x2);
            int y = R.Next(y1, y2);

            return new Point(x, y);
        }
    }

    public class Map {
        public int[,] MapData;
        public int[,] Explored;
        public Element FloorElement = Element.None;
        
        public List<Room> Rooms = new List<Room>();
        public List<IEntity> Entities = new List<IEntity>();
        [JsonIgnore]
        public List<TileType> Tiles = new List<TileType>();
        [JsonIgnore]
        public int startx;
        [JsonIgnore]
        public int starty;
        public int Floor = 1;
        private PlayerEntity Player;

        public List<Point> Vision = new List<Point>();

        public void Explore(PlayerEntity _Player) {
            this.Player = _Player;
            int x = _Player.x, y = _Player.y;
            double Radius = 3.5;
            int top = (int)Math.Max(0, y - Radius);
            int bottom = (int)Math.Min(Explored.GetLength(1), y + Radius);

            List<Point> ExploredCells = new List<Point>();

            for (int y2 = top; y2 < bottom; y2++) {
                int dy = y2 - y;
                int dx = (int)Math.Floor(Math.Sqrt(Radius * Radius - dy * dy));
                int left = Math.Max(0, x - dx);
                int right = Math.Min(Explored.GetLength(0), x + dx);
                for (int x2 = left; x2 < right; x2++) {
                    Explored[x2, y2] = 1;
                    ExploredCells.Add(new Point(x2, y2));
                }
            }

            var RM = GetRoom(x, y, true);
            if (RM != null) {
                for (int x2 = RM.x1 - 1; x2 <= RM.x2 + 1; x2++) {
                    for (int y2 = RM.y1 - 1; y2 <= RM.y2 + 1; y2++) {
                        Explored[x2, y2] = 1;
                        ExploredCells.Add(new Point(x2, y2));
                    }
                }
            }

            Vision = ExploredCells;
        }

        public Room GetRoom(int x, int y, bool includeDoors = false) {
            int Door = includeDoors ? 1 : 0;
            return Rooms.FirstOrDefault(frm => x >= (frm.x1 - Door) && x <= frm.x2 + Door && y >= frm.y1 - Door && y <= frm.y2 + Door);
        }

        public bool isTile(int x, int y, int[] type) {
            if (x < 0 || y < 0 || x > MapData.GetLength(0) - 1 || y > MapData.GetLength(1) - 1) return false;
            return type.Contains(MapData[x, y]);
        }

        public bool isSolid(int x, int y, bool includePlayer = true) {
            if (x < 0 || y < 0 || x > MapData.GetLength(0) - 1 || y > MapData.GetLength(1) - 1) return true;
            TileType TT = Tiles.Find(t => t.ID == MapData[x, y]);
            if (!TT.Walkable) {
                return true;
            }
            if (Entities.Find(e => e.x == x && e.y == y)?.Solid == true) {
                return true;
            }
            if (includePlayer == true && (
                    (Player != null && Player.x == x && Player.y == y) ||
                    (Player == null && startx == x && starty == y)
                )
            ) {
                return true;
            }

            return false;
        }

        public void GenerateTileTypes() {
			string F = File.ReadAllText(Directory.GetCurrentDirectory() + "/Content/TileTypes.json");
			Tiles = JsonConvert.DeserializeObject<List<TileType>>(F);
		}
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dungeon.Functionality {
    public static class PathFinder {
        public static Location FindPath(GameState GS, int startx, int starty, int endx, int endy) {
            var start = new Location { X = startx, Y = starty };
            var end = new Location { X = endx, Y = endy };

            Location Current = null;
            var openList = new List<Location>();
            var closedList = new List<Location>();
            int g = 0;
            openList.Add(start);
            while (openList.Count > 0) {
                var lowest = openList.Min(x => x.F);
                Current = openList.First(x => x.F == lowest);
                closedList.Add(Current);
                openList.Remove(Current);

                if (closedList.FirstOrDefault(l => l.X == end.X && l.Y == end.Y) != null) break;

                var adjacentSquares = GetWalkableAdjacentSquares(GS, Current.X, Current.Y, openList);
                g = Current.G + 1;

                foreach (var square in adjacentSquares) {
                    if (closedList.FirstOrDefault(l => l.X == square.X && l.Y == square.Y) != null) continue;

                    if (openList.FirstOrDefault(l => l.X == square.X && l.Y == square.Y) == null) {
                        square.G = g;
                        square.H = computeHScore(square.X, square.Y, end.X, end.Y);
                        square.F = square.G = square.H;
                        square.parent = Current;
                        openList.Insert(0, square);
                    } else {
                        if (g + square.H < square.F) {
                            square.G = g;
                            square.F = square.G + square.H;
                            square.parent = Current;
                        }
                    }
                }
            }

            while (true) {
                if (Current.parent?.parent != null) {
                    Current = Current.parent;
                } else {
                    break;
                }
            }
            return Current;
        }

        private static int computeHScore(int startx, int starty, int endx, int endy) {
            return Math.Abs(endx - startx) + Math.Abs(endy - starty);
        }

        public static List<Location> GetWalkableAdjacentSquares(GameState GS, int x, int y, List<Location> openList = null, bool includePlayer = false) {
            List<Location> list = new List<Location>();

            int[,] pos = new int[,] {
                {x, y - 1},
                {x, y + 1},
                {x - 1, y},
                {x + 1, y}
            };
            for (int i = 0; i < pos.GetLength(0); i++) {
                if (!GS.Map.isSolid(pos[i, 0], pos[i, 1], includePlayer)) {
                    Location node = openList?.Find(l => l.X == pos[i, 0] && l.Y == pos[i, 1]);
                    if (node == null) list.Add(new Location { X = pos[i, 0], Y = pos[i, 1] });
                    else list.Add(node);
                }
            }

            return list;
        }
    }
    public class Location {
        public int X;
        public int Y;
        public int F;
        public int G;
        public int H;
        public Location parent;
    }
}

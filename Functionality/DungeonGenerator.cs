﻿using Dungeon.Entities;
using Dungeon.Entities.Enemies;
using Dungeon.Entities.Items;
using Dungeon.Entities.Items.Consumable;
using Dungeon.Entities.Items.Equipment;
using Entities;
using freakcode.Cryptography;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Dungeon.Functionality {

	public class DungeonGenerator {
		public int Width = 30;
		public int Height = 30;
		private CryptoRandom R;

		public static Map Generate(int floor) {
			DungeonGenerator DG = new DungeonGenerator(floor);

			//DG.Map.GenerateTileTypes();
			DG.BuildRooms();
			while (DG.BuildMaze()) { };
			DG.BuildDoors();
			DG.TestConnectivity();
			DG.TrimDeadEnds();
			DG.BuildEnd();
			DG.Map.Explored = new int[DG.Map.MapData.GetLength(0), DG.Map.MapData.GetLength(1)];
			DG.BuildLockedDoors();
			DG.BuildTheme();

			return DG.Map;
		}

		

		private Map Map = new Map();

		public DungeonGenerator(int floor) {
			Map.Floor = floor;
			R = new CryptoRandom();
			Width = R.Next(20, 50);
			Height = R.Next(20, 50);
			Map.MapData = new int[Width, Height];
			Map.GenerateTileTypes();
		}

		private bool scanTiles(int x1, int y1, int x2, int y2) {
			if (x1 < 0 || y1 < 0 || x2 > Map.MapData.GetLength(0) - 1 || y2 > Map.MapData.GetLength(1) - 1) return false;
			for (int x = x1; x <= x2; x++) {
				for (int y = y1; y <= y2; y++) {
					if (Map.MapData[x, y] != 0) {
						return false;
					}
				}
			}
			return true;
		}

		private void BuildTheme() {
			//Theme Floors
			if (Map.Floor % 5 == 0 || R.Next(1, 100) > 85) {
				Map.FloorElement = Element.All[R.Next(0, Element.All.Length)];
			}

			Map.Entities.AddRange(ItemFactory.GenerateItems(Map));
			Map.Entities.AddRange(MonsterFactory.GenerateEnemies(Map));
		}

		public void BuildRooms() {
			int tries = 100;
			while (tries > 0) {
				int w = R.Next(4, 10), h = R.Next(3, 8);
				int x = R.Next(1, Width - w - 1), y = R.Next(1, Height - h - 1);

				if (scanTiles(x - 1, y - 1, x + w + 1, y + h + 1)) {
					Map.Rooms.Add(new Room(x, y, x + w - 1, y + h - 1));

					for (int roomx = x; roomx < x + w; roomx++) {
						for (int roomy = y; roomy < y + h; roomy++) {
							Map.MapData[roomx, roomy] = 2;
						}
					}
				} else {
					tries--;
				}
			}

			int index = R.Next(Map.Rooms.Count());
			Map.Rooms[index].IsStart = true;
			Map.startx = R.Next(Map.Rooms[index].x1, Map.Rooms[index].x2);
			Map.starty = R.Next(Map.Rooms[index].y1, Map.Rooms[index].y2);


			/*
            for(int i = 0; i < 5; i++) {
                var RandomRoom = Map.Rooms[R.Next(0, Map.Rooms.Count - 1)];
                int SignX = R.Next(RandomRoom.x1 + 1, RandomRoom.x2 - 1);
                int SignY = R.Next(RandomRoom.y1 + 1, RandomRoom.y2 - 1);
                if (i == 4) {
                    Map.Entities.Add(new ItemDrop{
                        x = SignX,
                        y = SignY,
                        Item = new Potion{ Health = 20 }
                    });
                } else {
                    Map.Entities.Add(MonsterFactory.GenerateEnemy<BatEntity>(
                        Map.Floor, 
                        SignX, 
                        SignY
                    ));
                }
            }
            */


		}

		public bool BuildMaze() {
			int x = -1, y = -1;
			bool FoundStart = false;
			//Island stopping
			int startTries = 1000;
			while (true) {
				if (startTries < 0) {
					break;
				}
				x = R.Next(1, Width - 1);
				y = R.Next(1, Height - 1);

				if (scanTiles(x - 1, y - 1, x + 1, y + 1)) {
					FoundStart = true;
					break;
				} else {
					startTries--;
				}
			}
			if (FoundStart == false) {
				return false;
			}

			Stack<Tuple<int, int>> LastMove = new Stack<Tuple<int, int>>();
			Map.MapData[1, 1] = 1;
			int lastDir = 0; //1 = up, 1 right, 1 down, 1 left
			double weight = 3.5d;

			while (true) {
				//Get Directions
				Dictionary<int, double> Dirs = new Dictionary<int, double>();

				if (scanTiles(x - 1, y - 2, x + 1, y - 1)) {
					Dirs.Add(1, R.NextDouble() * (1 + (lastDir == 1 ? weight : 0)));
				}
				if (scanTiles(x + 1, y - 1, x + 2, y + 1)) {
					Dirs.Add(2, R.NextDouble() * (1 + (lastDir == 2 ? weight : 0)));
				}
				if (scanTiles(x - 1, y + 1, x + 1, y + 2)) {
					Dirs.Add(3, R.NextDouble() * (1 + (lastDir == 3 ? weight : 0)));
				}
				if (scanTiles(x - 2, y - 1, x - 1, y + 1)) {
					Dirs.Add(4, R.NextDouble() * (1 + (lastDir == 4 ? weight : 0)));
				}

				if (Dirs.Count() > 0) {
					var NewDir = Dirs.OrderByDescending(item => item.Value).First().Key;
					if (NewDir == 1) {
						y--;
					} else if (NewDir == 2) {
						x++;
					} else if (NewDir == 3) {
						y++;
					} else if (NewDir == 4) {
						x--;
					}
					lastDir = NewDir;
					Map.MapData[x, y] = 1;
					LastMove.Push(new Tuple<int, int>(x, y));
				} else if (LastMove.Count > 0) {
					var p = LastMove.Pop();
					x = p.Item1;
					y = p.Item2;
				} else {
					break;
				}
			}
			return true;
		}

		private class DoorOption {
			public int x { get; set; }
			public int y { get; set; }
			public int dir { get; set; }

			public DoorOption(int _x, int _y, int _dir) {
				this.x = _x;
				this.y = _y;
				this.dir = _dir;
			}
		}

		private bool BuildRoomDoors(Room Rm) {
			List<DoorOption> Options = new List<DoorOption>();
			int Doors = (int)Math.Ceiling(R.NextDouble() * 2.1d);
			int[] breakthroughs = new int[] { 1, 2 };

			for (int x = Rm.x1; x < Rm.x2; x++) {
				if (Map.isTile(x, Rm.y1 - 2, breakthroughs) && scanTiles(x - 1, Rm.y1 - 1, x + 1, Rm.y1 - 1)) {
					Options.Add(new DoorOption(x, Rm.y1 - 1, 1));
				}
				if (Map.isTile(x, Rm.y2 + 2, breakthroughs) && scanTiles(x - 1, Rm.y2 + 1, x + 1, Rm.y2 + 1)) {
					Options.Add(new DoorOption(x, Rm.y2 + 1, 3));
				}
			}
			for (int y = Rm.y1; y < Rm.y2; y++) {
				if (Map.isTile(Rm.x1 - 2, y, breakthroughs) && scanTiles(Rm.x1 - 1, y - 1, Rm.x1 - 1, y + 1)) {
					Options.Add(new DoorOption(Rm.x1 - 1, y, 2));
				}
				if (Map.isTile(Rm.x2 + 2, y, breakthroughs) && scanTiles(Rm.x2 + 1, y - 1, Rm.x2 + 1, y + 1)) {
					Options.Add(new DoorOption(Rm.x2 + 1, y, 4));
				}
			}

			bool DoorAdded = false;
			for (int i = 0; i < Doors; i++) {
				if (Options.Count() > 0) {
					int index = R.Next(Options.Count());
					var opt = Options[index];
					Map.MapData[opt.x, opt.y] = 3;
					DoorAdded = true;
					Options.RemoveAll(x =>
						x.dir == opt.dir &&
						(x.x >= opt.x - 1 && x.x <= opt.x + 1 && x.y >= opt.y - 1 && x.y <= opt.y + 1)
					);
				}
			}
			return DoorAdded;
		}

		public void BuildDoors() {
			foreach (Room Rm in Map.Rooms) {
				BuildRoomDoors(Rm);
			}
		}

		public void TrimDeadEnds() {
			int[] walls = new int[] { 0 };

			while (true) {
				bool Found = false;
				for (int y = 0; y < Map.MapData.GetLength(1); y++) {
					for (int x = 0; x < Map.MapData.GetLength(0); x++) {
						int wallcount = 0;
						if (Map.MapData[x, y] == 1 || Map.MapData[x, y] == 3) {
							if (Map.isTile(x, y - 1, walls)) {
								wallcount++;
							}
							if (Map.isTile(x, y + 1, walls)) {
								wallcount++;
							}
							if (Map.isTile(x - 1, y, walls)) {
								wallcount++;
							}
							if (Map.isTile(x + 1, y, walls)) {
								wallcount++;
							}

							if (wallcount >= 3) {
								Map.MapData[x, y] = 0;
								Found = true;
							}
						}
					}
				}
				if (!Found) {
					break;
				}
			}
		}

		private int[,] GetExploration() {
			Room start = Map.Rooms.Find(x => x.IsStart);
			int[] Walkables = new int[] { 1, 2, 3 };
			int[,] Exploration = new int[Width, Height];
			Exploration[start.x1, start.y1] = 1;

			while (true) {
				bool FoundPath = false;

				/*
                    * 0 Unknown
                    * 1 Searching
                    * -1 Blocked
                    * 2 Searched
                    */

				for (int x = 0; x < Exploration.GetLength(0) - 1; x++) {
					for (int y = 0; y < Exploration.GetLength(1) - 1; y++) {
						if (Exploration[x, y] == 1) {
							if (Exploration[x, y - 1] == 0) {
								Exploration[x, y - 1] = Map.isTile(x, y - 1, Walkables) ? 1 : -1;
							}
							if (Exploration[x - 1, y] == 0) {
								Exploration[x - 1, y] = Map.isTile(x - 1, y, Walkables) ? 1 : -1;
							}
							if (Exploration[x + 1, y] == 0) {
								Exploration[x + 1, y] = Map.isTile(x + 1, y, Walkables) ? 1 : -1;
							}
							if (Exploration[x, y + 1] == 0) {
								Exploration[x, y + 1] = Map.isTile(x, y + 1, Walkables) ? 1 : -1;
							}
							Exploration[x, y] = 2;
							FoundPath = true;
						}
					}
				}
				if (!FoundPath) {
					break;
				}
			}

			return Exploration;
		}

		private void BuildLockedDoors() {
			int[] Walkables = new int[] { 1, 2, 3 };
			Room start = Map.Rooms.Find(x => x.IsStart);
			

			Dictionary<Room, int[,]> SingleDoorRooms = new Dictionary<Room, int[,]>();
			foreach(Room r in Map.Rooms) {
				if (r.IsStart) continue;
				int doorCount = 0;
				int[,] DoorCoord = new int[1,2];
				for (int x = r.x1 - 1; x <= r.x2 + 1; x++) {
					for (int y = r.y1-1; y <= r.y2+1; y++) {
						if (Map.MapData[x, y] == 3) {
							doorCount++;
							DoorCoord[0,0] = x;
							DoorCoord[0,1] = y;
						}
					}
				}
				if (doorCount == 1 && R.Next(1,100) > 60) {
					SingleDoorRooms.Add(r, DoorCoord);
					r.IsTreasureRoom = true;
				}
			}
			
			List<Door> DoorEntities = new List<Door>();
			foreach(var SDR in SingleDoorRooms) {
				List<Room> RoomsAheadOfDoor = new List<Room>();
			
				int[,] Exploration = new int[Width, Height];
				Exploration[start.x1, start.y1] = 1;

				Door D = new Door{
					x = SDR.Value[0,0],
					y = SDR.Value[0,1]
				};
				Map.Entities.Add(D);
				DoorEntities.Add(D);

				while (true) {
					bool FoundPath = false;
					/*
						* 0 Unknown
						* 1 Searching
						* -1 Blocked
						* 2 Searched,
						* 3 Locked Door
						*/

					for (int x = 0; x < Exploration.GetLength(0) - 1; x++) {
						for (int y = 0; y < Exploration.GetLength(1) - 1; y++) {
							if (Exploration[x, y] == 1) {
								int[,] pos = new int[,] {
									{x, y - 1},
									{x, y + 1},
									{x - 1, y},
									{x + 1, y}
								};
								for (int i = 0; i < pos.GetLength(0); i++) {
									if (Exploration[pos[i,0], pos[i,1]] == 0) {
										var isSolid = Map.isSolid(pos[i,0], pos[i,1], false);
										var type = 1;
										if (isSolid) {
											type = -1;
											if (Map.MapData[pos[i,0], pos[i,1]] == 3) {
												type = 3;
											}
										}
										Exploration[pos[i,0], pos[i,1]] = type;
									}
								}
								var currentroom = Map.GetRoom(x, y, false);
								if (currentroom != null && !RoomsAheadOfDoor.Contains(currentroom)) {
									RoomsAheadOfDoor.Add(currentroom);
								}
								Exploration[x, y] = 2;
								FoundPath = true;
							}
						}
					}
					if (!FoundPath) {
						break;
					}
				}

				var KeyRoom = RoomsAheadOfDoor.OrderBy(x => R.Next(0,100)).First();
				var keyx = R.Next(KeyRoom.x1, KeyRoom.x2);
				var keyy = R.Next(KeyRoom.y1, KeyRoom.y2);
				Map.Entities.Add(new DoorKey {
					x = keyx,
					y = keyy
				});
				Map.Entities.Remove(D);

			}
			
			Map.Entities.AddRange(DoorEntities);
		}

		public void TestConnectivity(int tries = 0) {
			int[,] Exploration = GetExploration();
			bool unExplored = false;

			for (int i = 0; i < Map.Rooms.Count(); i++) {
				Room rm = Map.Rooms[i];
				if (Exploration[rm.x1, rm.y1] != 2) {
					rm.WasOrphaned = true;
					if (!BuildRoomDoors(rm) && tries > 100) {
						for (int x = rm.x1 - 1; x <= rm.x2 + 1; x++) {
							for (int y = rm.y1 - 1; y <= rm.y2 + 1; y++) {
								Map.MapData[x, y] = 0;
							}
						}
						Map.Rooms.RemoveAt(i);
						i--;
					} else {
						unExplored = true;
					}
					Exploration = GetExploration();
					//i--;
				}
			}
			if (unExplored) {
				TestConnectivity(tries + 1);
			}
		}

		public Room GetRoom(int x, int y, bool includeDoors = false) {
			int Door = includeDoors ? 1 : 0;
			return Map.Rooms.FirstOrDefault(frm => x >= (frm.x1 - Door) && x <= frm.x2 + Door && y >= frm.y1 - Door && y <= frm.y2 + Door);
		}

		private void BuildEnd() {
			while (true) {
				var index = R.Next(Map.Rooms.Count());
				var E = Map.Rooms[index];
				Map.Rooms[index].IsEnd = true;
				int StairX = R.Next(E.x1 + 1, E.x2 - 1);
				int StairY = R.Next(E.y1 + 1, E.y2 - 1);
				Map.Entities.Add(new StairEntity {
					x = StairX,
					y = StairY
				});
				break;
			}
		}
	}
}
﻿using Dungeon.Entities;
using Dungeon.Entities.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dungeon.Functionality {
    public class GameState {
        public Map Map;
        public List<MessageEntity> Messages = new List<MessageEntity>();
        public PlayerEntity Player = new PlayerEntity();
        public Rectangle ScreenRect = new Rectangle();
        public List<IItems> Inventory = new List<IItems>();
        public int Money {get; set;}

        public bool InMessage {
            get {
                return Messages.Count != 0 && Messages.Last().Answered == false;
            }
        }

        public void UpdateScreenRect() {

            ScreenRect.x1 = Player.x - 12;
            ScreenRect.x2 = Player.x + 12;
            ScreenRect.y1 = Player.y - 6;
            ScreenRect.y2 = Player.y + 6;
        }

        public void EnterFloor() {
            int Floor = 1;
            if (Map != null) {
                Floor = Map.Floor + 1;
            }
            Map = DungeonGenerator.Generate(Floor);
            var Start = Map.Rooms.First(x => x.IsStart);
            Player.x = Map.startx;
            Player.y = Map.starty;
            //Remove old floor keys
            Inventory.RemoveAll(x => x is DoorKey);
            Map.Explore(Player);
            UpdateScreenRect();
        }
    }

    public class Rectangle {
        public int x1;
        public int x2;
        public int y1;
        public int y2;

        public bool isInside(int x, int y) {
            return (x >= x1 && x <= x2 && y >= y1 && y <= y2);
        }
    }
}

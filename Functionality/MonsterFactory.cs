using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Dungeon.Entities;
using Newtonsoft.Json;
using freakcode.Cryptography;

namespace Dungeon.Functionality
{
    public static class MonsterFactory
    {
        private static List<MonsterEntry> Monsters = null;
        private static void InitiateEncyclopedia() {
            if (Monsters == null) {
                string datafile = File.ReadAllText(Directory.GetCurrentDirectory() + "/Content/MonsterEncyclopedia.json");
                Monsters = JsonConvert.DeserializeObject<List<MonsterEntry>>(datafile);
            }
        }
        [Obsolete]
        public static IEnemy GenerateEnemy<T>(int Level, int X, int Y) where T : IEnemy, new() {
            var enemy = new T();
            enemy.x = X;
            enemy.y = Y;
            ((LifeForm)(object)enemy).LVL = Level;
            ((LifeForm)(object)enemy).HP = ((LifeForm)(object)enemy).MAX_HP;
            return enemy;
        }
        
        public static IEnemy GenerateEnemy(MonsterEntry Monster, int Level, int X, int Y) {
            var enemy = (IEnemy)Activator.CreateInstance(Monster.Type);
            enemy.x = X;
            enemy.y = Y;
            enemy.Name = Monster.Name;
            enemy.KillEXP = Monster.KillExp;
            enemy.MoneyDrop = Monster.MoneyDrop;
            var LifeForm = ((LifeForm)(object)enemy);
            LifeForm.Base = Monster.GetBaseStats();
            LifeForm.LVL = Level;
            LifeForm.HP = LifeForm.MAX_HP;
            LifeForm.Elements = Monster.Elements?.ToList();
            return enemy;
        }

        public static List<IEnemy> GenerateEnemies(Map map) {
            InitiateEncyclopedia();
            List<IEnemy> Enemies = new List<IEnemy>();
            CryptoRandom R = new CryptoRandom();
            int amount = R.Next(Convert.ToInt32(map.Rooms.Count * .25), map.Rooms.Count);
            while (amount > 0) {
                var newEnemy = Monsters
                    .Where(x => x.Floors == null || x.Floors.Contains(map.Floor))
                    .OrderByDescending(x => R.Next(0, x.Rarity) + (x.Elements?.Contains(map.FloorElement) == true ? 200 : 0))
                    .First();
                Room room = map.Rooms[R.Next(0, map.Rooms.Count)];
                Point p = room.getRandomPoint();
                if (!map.isSolid(p.x, p.y)) {
                    Enemies.Add(GenerateEnemy(newEnemy, map.Floor, p.x, p.y));
                    amount--;
                }
            }

            return Enemies;
        }
    }

    public class MonsterEntry {
        [JsonConverter(typeof(MonsterTypeConverter))]
        public Type Type {get; set;}
        public string Name {get; set;}
        public int HP {get; set;}
        public int ATK {get; set;}
        public int DEF {get; set;}
        public int SPATK {get; set;}
        public int SPDEF {get; set;}
        public int KillExp {get; set;}
        [JsonConverter(typeof(MonsterElementConverter))]
        public Element[] Elements {get; set;}
        public int[] Floors {get; set;}
        public int Rarity {get; set;}
        public int MoneyDrop {get; set;}

        public BaseStats GetBaseStats() {
            return new BaseStats(HP, ATK, DEF, SPATK, SPDEF);
        }
    }
	public class MonsterTypeConverter : JsonConverter {
		public override bool CanConvert(Type objectType) {
			return true;
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
			return Type.GetType("Dungeon.Entities.Enemies." + serializer.Deserialize<string>(reader) + "Entity");
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
			throw new NotImplementedException();
		}
	}
	public class MonsterElementConverter : JsonConverter {
		public override bool CanConvert(Type objectType) {
			return true;
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
            string[] data = serializer.Deserialize<string[]>(reader);
            if (data == null) return null;
            Element[] Elements = new Element[data.Length];
            for(int i = 0; i < data.Length; i++) {
                Type t = typeof(Element);
                Elements[i] = (Element)t.GetField(data[i]).GetValue(null);
            }
            
			return Elements;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
			throw new NotImplementedException();
		}
	}
}
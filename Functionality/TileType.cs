﻿using System;

namespace Dungeon.Functionality {

    public class TileType {

        public class TileColor {
            public ConsoleColor? BG { get; set; }
            public ConsoleColor? FG { get; set; }
            public TileColor Vision { get; set; }
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public bool Walkable { get; set; }
        public char C { get; set; }
        public TileColor Color { get; set; }

        public TileType() {
            this.Color = new TileColor();
        }
    }
}
﻿using Dungeon.Entities;
using Dungeon.Entities.Items.Equipment;
using System;
using System.Linq;

namespace Dungeon.Functionality {

    public static class BattleEngine {

        private static Random R  = new Random();

        public static int Attack(LifeForm Attacker, LifeForm Defender) {
            int Attack = 0;
            int Defense = 0;
            IEquipment Weapon = Attacker.Equipment[SlotType.Weapon];
            if (Weapon == null) Weapon = new Fist();
            Element Ele = Weapon.Element;
            if (Ele == null)Ele = Element.None;
            if (Weapon.ATKStat == AttackStat.Physical) {
                Attack = Attacker.ATK;
                Defense = Defender.DEF;
            } else if (Weapon.ATKStat == AttackStat.Special) {
                Attack = Attacker.SPATK;
                Defense = Defender.SPDEF;
            }
            double Multiplier = 1;
            for (int i = 0; i < Defender.Elements?.Where(x => x.Resist.Contains(Weapon.Element)).Count(); i++) {
                Multiplier /= 2;
            }
            for (int i = 0; i < Defender.Elements?.Where(x => x.Weakness.Contains(Weapon.Element)).Count(); i++) {
                Multiplier *= 2;
            }
            double b = (2*(double)Attacker.LVL / 5) + 2;
            double c = ((double)Attack / (double)Defense);
            double d = b * c * Multiplier * ((double)R.Next(85, 100) / 100);
            int a = Convert.ToInt32(Math.Round(d));
            int DamageDealt = a;
            if (DamageDealt < 0) {
                DamageDealt = 0;
            }
            Defender.HP -= DamageDealt;
            return DamageDealt;
        }
    }

    public class Attack {
        public AttackStat Stat { get; set; }
        public AttackType Type { get; set; }
        public Element Element { get; set; }
    }

    public enum AttackStat {
        Physical,
        Special
    }

    public enum AttackType {
        Slash,
        Stab,
        Elemental,
        Slam
    }

    public class Element : IEquatable<Element> {

        public static readonly Element None = new Element {
            Name = "None",
            Color = ConsoleColor.White
        };

        public static readonly Element Water = new Element {
            Name = "Water",
            Weakness = new Element[] { Element.Ice, Element.Electric },
            Resist = new Element[] { Element.Fire, Element.Water },
            Color = ConsoleColor.DarkBlue
        };

        public static readonly Element Fire = new Element {
            Name = "Fire",
            Weakness = new Element[] { Element.Water },
            Resist = new Element[] { Element.Ice, Element.Fire },
            Color = ConsoleColor.Red
        };

        public static readonly Element Ice = new Element {
            Name = "Ice",
            Resist = new Element[] { Element.Water },
            Weakness = new Element[] { Element.Fire },
            Color = ConsoleColor.Blue
        };

        public static readonly Element Electric = new Element {
            Name = "Electric",
            Resist = new Element[] { },
            Weakness = new Element[] { Element.Water },
            Color = ConsoleColor.DarkYellow
        };

        public static readonly Element Dark = new Element {
            Name = "Dark",
            Resist = new Element[] { Element.None, Element.Dark },
            Weakness = new Element[] { Element.Light },
            Color = ConsoleColor.DarkMagenta
        };

        public static readonly Element Light = new Element {
            Name = "Light",
            Resist = new Element[] { Element.None, Element.Light },
            Weakness = new Element[] { Element.Dark },
            Color = ConsoleColor.Yellow
        };

        public static readonly Element Earth = new Element {
            Name = "Earth",
            Resist = new Element[] { Element.None, Element.Electric },
            Color = ConsoleColor.DarkGreen
        };
        public static readonly Element Undead = new Element {
            Name = "Undead",
            Resist = new Element[] { Element.Fire, Element.Light },
            Color = ConsoleColor.Green
        };

        public static readonly Element[] All = new Element[] {
            Water, Fire, Ice, Electric, Dark, Light, Earth, Undead
        };

        public string Name { get; set; }
        public Element[] Weakness { get; set; } = new Element[] { };
        public Element[] Resist { get; set; } = new Element[] { };
        public ConsoleColor Color {get; set;}

        public bool Equals(Element other) {
            return this.Name == other.Name;
        }
    }
}
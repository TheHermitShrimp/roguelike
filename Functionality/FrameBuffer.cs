﻿using System;
using System.Linq;

namespace Dungeon.Functionality {

    public class FrameBuffer {
        private const int Width = 80;
        private const int Height = 20;
        private const int Area = Width * Height;

        private char[] _last = new char[Area];
        private char[] _chars = new char[Area];
        private ConsoleColor[,] _lastcolors = new ConsoleColor[Area, 2];
        private ConsoleColor[,] _colors = new ConsoleColor[Area, 2];

        public void SetPixel(int x, int y, char character) {
            _chars[y * Width + x] = character;
        }

        public void SetForeground(int x, int y, ConsoleColor FG) {
            int index = y * Width + x;
            if (index >= 0 && index <= _colors.GetLength(0)) {
                _colors[index, 0] = FG;
            }
        }

        public void SetBackground(int x, int y, ConsoleColor BG) {
            int index = y * Width + x;
            if (index >= 0 && index <= _colors.GetLength(0)) {
                _colors[index, 1] = BG;
            }
        }

        public void Clear() {
            for (int i = 0; i < Area; i++) {
                _chars[i] = ' ';
                _colors[i, 0] = ConsoleColor.White;
                _colors[i, 1] = ConsoleColor.Black;
            }
        }

        public void WriteString(int x, int y, string data, ConsoleColor FG = ConsoleColor.White, ConsoleColor BG = ConsoleColor.Black) {
            if (data == null) return;
            char[] d = data.ToCharArray();
            for (int i = 0; i < d.Count(); i++) {
                _chars[y * Width + x + i] = d[i];
                _colors[y * Width + x + i, 1] = BG;
                _colors[y * Width + x + i, 0] = FG;
            }
        }

        public void Render() {
            for (int i = 1; i < _chars.Length; i++) {
                if (_chars[i] != _last[i] || _colors[i, 0] != _lastcolors[i, 0] || _colors[i, 1] != _lastcolors[i, 1]) {
                    Console.SetCursorPosition(i % Width, i / Width);
                    Console.ForegroundColor = _colors[i, 0];
                    Console.BackgroundColor = _colors[i, 1];
                    Console.Write(_chars[i]);
                }
            }
            Console.SetCursorPosition(0, 0);
            _last = (char[])_chars.Clone();
            _lastcolors = (ConsoleColor[,])_colors.Clone();
        }
    }
}
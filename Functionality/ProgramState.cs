﻿using Dungeon.Screens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dungeon.Functionality {
    public class ProgramState {
        public Stack<IScreen> Screens = new Stack<IScreen>();
        public FrameBuffer Buffer = new FrameBuffer();
        public bool Running = true;
        public ConsoleKeyInfo? KeyInfo = null;

        public IScreen CurrentScreen {
            get {
                return Screens.Peek();
            }
        }

        public void Update() {
            if (Console.KeyAvailable) {
                KeyInfo = Console.ReadKey(intercept: true);
            } else {
                KeyInfo = null;
            }
            CurrentScreen.Update(this);
        }
        public void Draw() {
            Buffer.Clear();
            CurrentScreen.Draw(ref Buffer);
            Buffer.Render();
        }

        public void Run() {
            Screens.Push(new MainMenu());
            while (Running) {
                this.Update();
                this.Draw();
            }
        }

        public bool isKeyPressed(ConsoleKey KI) {
            return KeyInfo?.Key == KI;
        }
    }
}

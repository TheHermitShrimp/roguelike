﻿using System.Collections.Generic;

namespace Dungeon.Extensions {

    public static class StringExtensions {

        public static string[] SplitAt(this string source, int index) {
            List<string> output = new List<string>();
            int pos = 0;

            while (true) {
                if (pos >= source.Length) {
                    break;
                }

                if (pos + index > source.Length) {
                    index = source.Length - pos;
                }
                output.Add(source.Substring(pos, index));
                pos += index;
            }
            return output.ToArray();
        }
    }
}
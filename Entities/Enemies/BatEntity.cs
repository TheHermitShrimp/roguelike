using System;
using System.Linq;
using Dungeon.Functionality;

namespace Dungeon.Entities.Enemies {
	public class BatEntity : LifeForm, IEnemy {
		public int KillEXP {get;set;}
		public int MoneyDrop { get;set; }

		public int x { get; set; }
		public int y { get; set; }
		public string Name { get; set; }
		public bool Solid { get; set; } = true;

        private Random R = new Random();

		public void Action(GameState GS, bool onTop) {
			GS.Messages.Add(new MessageEntity("You see a pesky bat."));
		}

		public void Draw(GameState GS, FrameBuffer FB) {
			DrawLifeForm(GS, FB, x, y, 'B');
		}
		int waitTimer = 0;
		public void Update(GameState GS) {
			if (HP <= 0) {
				GS.Map.Entities.Remove(this);
				return;
			}
			var Options = PathFinder.GetWalkableAdjacentSquares(GS,x,y,null, true);
			if (waitTimer > 0) {
				waitTimer--;
				return;
			}
			if (GS.Player.isNextTo(x,y)) {
				if (R.Next(100) > 85) {
					int DamageDealt = BattleEngine.Attack(this, GS.Player);

					GS.Messages.Add(new MessageEntity($"{Name} dealt {DamageDealt} damage to you."));
					waitTimer = 2;
					return;
				}
			}
            if (Options.Count > 0) {
				int OptIndex = R.Next(0, Options.Count);
                var Opt = Options[OptIndex];
                x = Opt.X;
                y = Opt.Y;
            }
		}
	}
}
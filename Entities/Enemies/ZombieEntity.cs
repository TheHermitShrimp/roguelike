﻿using Dungeon.Functionality;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dungeon.Entities.Enemies {
	public class ZombieEntity : LifeForm, IEnemy {
		//IEntity
		public int x { get; set; }
		public int y { get; set; }
		public string Name { get; set; }
		public bool Solid { get; set; } = true;

		//IEnemy
		public int KillEXP { get;set; }
		public int MoneyDrop { get;set; }

		//Zombie
		private bool Alerted = false;

		public void Action(GameState GS, bool onTop) {
			GS.Messages.Add(new MessageEntity("That's one ugly zombie."));
		}

		private int waitTimer = 0;
		public void Update(GameState GS) {
			if (HP <= 0) {
				GS.Map.Entities.Remove(this);
				return;
			}
			if (!Alerted && GS.Map.Vision.Contains(new Point(x, y))) {
				Alerted = true;
			} else if (Alerted && waitTimer <= 0) {
				Location Next = PathFinder.FindPath(GS, x, y, GS.Player.x, GS.Player.y);
				if (Next != null) {
					if (Next.X == GS.Player.x && Next.Y == GS.Player.y) {
						int DamageDealt = BattleEngine.Attack(this, GS.Player);

						GS.Messages.Add(new MessageEntity($"{Name} dealt {DamageDealt} damage to you."));
					} else {
						x = Next.X;
						y = Next.Y;
					}
				}
				waitTimer = 2;
			}
			waitTimer--;
		}

		public void Draw(GameState GS, FrameBuffer FB) {
			DrawLifeForm(GS, FB, x, y, 'Z');
			//FB.SetPixel(x - GS.ScreenRect.x1 + 1, y - GS.ScreenRect.y1 + 1, 'Z');
		}
	}
}

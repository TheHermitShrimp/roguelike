﻿using Dungeon.Functionality;

namespace Dungeon.Entities {

    public class SignEntity : IEntity {
        public int x { get; set; }
        
        public int y { get; set; }
        public string Name { get; set; } = "Sign";

        public bool Solid { get; set; } = true;
        private char Character = '♪';
        public string Message = "You got a splinter on a sign. You take 5 damage.";

        public void Action(GameState GS, bool onTop) {
            GS.Messages.Add(new MessageEntity(Message));
            GS.Player.HP -= 5;
        }

        public void Draw(GameState GS, FrameBuffer FB) {
            FB.SetPixel(x - GS.ScreenRect.x1 + 1, y - GS.ScreenRect.y1 + 1, Character);
        }

        public void Update(GameState GS) {}
    }
}
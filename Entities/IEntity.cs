﻿using Dungeon.Functionality;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dungeon.Entities {
    public interface IEntity {
        int x { get; set; }
        int y { get; set; }
        string Name { get; set; }
        bool Solid { get; set; }
        void Action(GameState GS, bool onTop);
        void Update(GameState GS);
        void Draw(GameState GS, FrameBuffer FB);
    }
}

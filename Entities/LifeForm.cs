﻿using Dungeon.Entities.Items.Equipment;
using Dungeon.Functionality;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dungeon.Entities
{
	public class LifeForm
	{
		//Health
		public int HP = 1;
		//Max Health
		[JsonIgnore]
		public int MAX_HP
		{
			get
			{
				return GetStat("HP");
			}
		}
		//MP
		public int MP { get; set; } = 0;
		//Max MP
		public int MAX_MP { get; set; } = 0;
		//Level
		public int LVL = 1;
		//Current Experience
		public int EXP { get; set; } = 0;
		//Attack
		[JsonIgnore]
		public int ATK
		{
			get
			{
				return GetStat("ATK");
			}
		}
		//Defence
		[JsonIgnore]
		public int DEF
		{
			get
			{
				return GetStat("DEF");
			}
		}
		//Magic Attack
		[JsonIgnore]
		public int SPATK
		{
			get
			{
				return GetStat("SPATK");
			}
		}
		//Magic Defense
		[JsonIgnore]
		public int SPDEF
		{
			get
			{
				return GetStat("SPDEF");
			}
		}

		public Dictionary<SlotType, IEquipment> Equipment = new Dictionary<SlotType, IEquipment>() {
			{SlotType.Weapon, null},
			{SlotType.Helmet, null},
			{SlotType.Armor, null},
			{SlotType.Boots, null},
			{SlotType.Etc1, null},
			{SlotType.Etc2, null}
		};

		public BaseStats Base = new BaseStats();
		// How often it can move in relation to the player.
		[JsonIgnore]
		public int SPD { get; set; } = 1;

		public List<Element> Elements = new List<Element>();

		public int GainExp(LifeForm Enemy, int AddExp)
		{
			int TotalExp = Convert.ToInt32(
				((double)(Enemy.LVL * AddExp) / 5) *
				(Math.Pow(2 * Enemy.LVL + 10, 2.5) / Math.Pow(Enemy.LVL + LVL + 10, 2.5d)) + 1);
			int retExp = TotalExp * 1;
			while (TotalExp > 0)
			{
				int LevelUpExp = GetNextExp();
				if (EXP + TotalExp >= LevelUpExp)
				{
					HP += GetStat("HP", LVL + 1) - GetStat("HP", LVL);
					LVL++;
					if (HP > MAX_HP)
					{
						HP = MAX_HP;
					}
					TotalExp -= LevelUpExp - EXP;
					EXP = 0;
				}
				else
				{
					EXP += TotalExp;
					break;
				}
			}
			return retExp;
		}

		internal int GetNextExp()
		{
			return Convert.ToInt32(Math.Round((double)(4 * Math.Pow(LVL, 3)) / 5));
		}

		internal int GetStat(string Type, int Level = 0)
		{
			int BaseNum = 0;
			if (Level == 0) Level = LVL;
			switch (Type) {
				case "HP": BaseNum = Base.HP; break;
				case "ATK": BaseNum = Base.ATK; break;
				case "DEF": BaseNum = Base.DEF; break;
				case "SPATK": BaseNum = Base.SPATK; break;
				case "SPDEF": BaseNum = Base.SPDEF; break;
			}
			BaseNum = ((2 * BaseNum * Level) / 100) + Level + 10;
			foreach(var item in Equipment) {
				if (item.Value != null) {
					switch (Type) {
						case "HP": BaseNum += item.Value.HP; break;
						case "ATK": BaseNum += item.Value.ATK; break;
						case "DEF": BaseNum += item.Value.DEF; break;
						case "SPATK": BaseNum += item.Value.SPATK; break;
						case "SPDEF": BaseNum += item.Value.SPDEF; break;
					}
				}
			}
			return BaseNum;
		}

		public void DrawLifeForm(GameState GS, FrameBuffer FB, int x, int y, char C) {
			FB.SetPixel(x - GS.ScreenRect.x1 + 1, y - GS.ScreenRect.y1 + 1, C);
			if (Elements != null && Elements.Count > 0) {
				Element E = Elements.First();
				FB.SetForeground(x - GS.ScreenRect.x1 + 1, y - GS.ScreenRect.y1 + 1, E.Color);
			}
		}
	}

	public class BaseStats
	{
		public int HP { get; } = 100;
		public int ATK { get; } = 100;
		public int DEF { get; } = 100;
		public int SPATK { get; } = 100;
		public int SPDEF { get; } = 100;

		public BaseStats() { }
		public BaseStats(int _HP, int _ATK, int _DEF, int _SPATK, int _SPDEF)
		{
			this.HP = _HP;
			this.ATK = _ATK;
			this.DEF = _DEF;
			this.SPATK = _SPATK;
			this.SPDEF = _SPDEF;
		}
	}
}

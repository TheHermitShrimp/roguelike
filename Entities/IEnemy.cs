﻿namespace Dungeon.Entities {
	public interface IEnemy : IEntity {
		int KillEXP { get; set; }
		int MoneyDrop {get; set;}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dungeon.Functionality;

namespace Dungeon.Entities.Items.Equipment {
	public interface IEquipment : IItems {
		SlotType[] Slots { get; set; }


		int HP { get; set; }
		int ATK { get; set; }
		int DEF { get; set; }
		int SPATK { get; set; }
		int SPDEF { get; set; }

		AttackType ATKType { get; set; }
		AttackStat ATKStat { get; set; }
		Element Element { get; set; }
	}

	public enum SlotType {
		Weapon,
		Armor,
		Helmet,
		Boots,
		Etc1,
		Etc2
	}
}

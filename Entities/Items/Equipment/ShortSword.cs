using Dungeon.Functionality;

namespace Dungeon.Entities.Items.Equipment {
	public class ShortSword : IEquipment {
		public SlotType[] Slots { get; set; } = new SlotType[] { SlotType.Weapon };
		public int HP { get; set; } = 0;
		public int ATK { get; set; } = 10;
		public int DEF { get; set; } = 0;
		public int SPATK { get; set; } = 0;
		public int SPDEF { get; set; } = 0;
		public AttackType ATKType { get; set; } = AttackType.Slash;
		public AttackStat ATKStat { get; set; } = AttackStat.Physical;
		public Element Element { get; set; } = Element.None;
		public ItemType Type => ItemType.Equipment;

		public string Name { get; set; } = "Short Sword";
		public string Description { get; set; } = "A small sword.";

		public char Icon {get;set;} = 'W';
	}
}
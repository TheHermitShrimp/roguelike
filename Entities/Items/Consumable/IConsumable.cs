using Dungeon.Functionality;

namespace Dungeon.Entities.Items.Consumable {
    public interface IConsumable : IItems
    {
        bool Use(GameState GS);
    }
}
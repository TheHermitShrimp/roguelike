using System;
using Dungeon.Functionality;

namespace Dungeon.Entities.Items.Consumable
{
	public class Potion : IConsumable
	{
		public ItemType Type {get;} = ItemType.Consumable;

		public string Name {get; set;} = "Potion";

		public string Description { 
			get {
				return $"Restores {Health} HP instantly.";
			}
			set {

			}
		}

		public char Icon {get;set;} = 'P';

		public int Health {get;set;} =  20;

		public bool Use(GameState GS)
		{
			if (GS.Player.HP == GS.Player.MAX_HP) {
				GS.Messages.Add(new MessageEntity("You are already at full health"));
				return false;
			} else {
				int NewHealth = Math.Min(GS.Player.MAX_HP, GS.Player.HP + this.Health);
				GS.Messages.Add(new MessageEntity($"You healed {NewHealth - GS.Player.HP} HP."));
				GS.Player.HP = NewHealth;
				return true;
			}
		}
	}
}
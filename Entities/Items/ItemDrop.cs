using Dungeon.Functionality;

namespace Dungeon.Entities.Items {
	public class ItemDrop : IEntity {
		public int x { get; set; }
		public int y { get; set; }
		public string Name {
			get {
				return Item.Name;
			}
			set { }
		}
		public bool Solid { get; set; } = false;

		public IItems Item { get; set; }
		public ItemDrop(){}
		public ItemDrop(IItems item, Point p) {
			this.Item = item;
			this.x = p.x;
			this.y = p.y;
		}

		public void Action(GameState GS, bool onTop) {
			if (onTop) {
				GS.Inventory.Add(this.Item);
				GS.Messages.Add(new MessageEntity($"Picked up 1 {this.Item.Name}."));
				GS.Map.Entities.Remove(this);
			} else {
				GS.Messages.Add(new MessageEntity($"You see a {this.Item.Name}."));
			}
		}

		public void Draw(GameState GS, FrameBuffer FB) {
			FB.SetPixel(x - GS.ScreenRect.x1 + 1, y - GS.ScreenRect.y1 + 1, Item.Icon);
		}

		public void Update(GameState GS) { }
	}
}
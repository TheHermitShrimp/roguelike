using Dungeon.Functionality;

namespace Dungeon.Entities.Items
{
	public class DoorKey : IEntity, IItems {
		public int x {get; set;}
		public int y {get; set;}
		public string Name {get; set;} = "Key";
		public bool Solid {get; set;} = false;
		public ItemType Type => ItemType.Other;

		public string Description {get; set;} = "A key for a lock.";

		public char Icon {get;set;} = 'ⱡ';

		public void Action(GameState GS, bool onTop) {
			if (onTop) {
				GS.Inventory.Add(this);
				GS.Messages.Add(new MessageEntity($"Picked up 1 {Name}."));
				GS.Map.Entities.Remove(this);
			} else {
				GS.Messages.Add(new MessageEntity($"You see a {Name}."));
			}
		}

		public void Draw(GameState GS, FrameBuffer FB) {
			FB.SetPixel(x - GS.ScreenRect.x1 + 1, y - GS.ScreenRect.y1 + 1, Icon);
		}

		public void Update(GameState GS) {}
	}
}
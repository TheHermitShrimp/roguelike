namespace Dungeon.Entities.Items
{
    public interface IItems
    {
        ItemType Type {get;}
        string Name {get; set;}
        string Description {get; set;}
        char Icon {get;set;}
    }

    public enum ItemType {
        Consumable,
        Equipment,
        Other
    }
}
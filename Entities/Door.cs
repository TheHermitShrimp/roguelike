using Dungeon.Entities;
using Dungeon.Entities.Items;
using Dungeon.Functionality;
using System.Collections.Generic;
using System.Linq;

namespace Entities
{
	public class Door : IEntity {
		public int x {get; set;}
		public int y {get; set;}
		public string Name {get; set;} = "Locked Door";
		public bool Solid {get; set;}= true;

		public void Action(GameState GS, bool onTop) {
            DoorKey Key = (DoorKey)GS.Inventory.FirstOrDefault(x => x is DoorKey);
			if (Key != null) {
                var ME = new MessageEntity("Use a key on the locked door?");
                ME.Choices = new List<ChoiceEntity> {
                        new ChoiceEntity {
                            Answer = "Yes",
                            OnSelected = () => {
                                GS.Messages.Add(new MessageEntity("The door has been unlocked."));
                                GS.Map.Entities.Remove(this);
                                ME.Answered = true;
                                GS.Inventory.Remove(Key);
                            },
                            Hovered = true
                        },
                        new ChoiceEntity {
                            Answer = "No",
                            OnSelected = () => {
                                ME.Answered = true;
                            }
                        }
                    
                };
                GS.Messages.Add(ME);
            } else {
                GS.Messages.Add(new MessageEntity("I'll need a key to open this door."));
            }
		}

		public void Draw(GameState GS, FrameBuffer FB) {
			FB.SetPixel(x - GS.ScreenRect.x1 + 1, y - GS.ScreenRect.y1 + 1, 'X');
		}

		public void Update(GameState GS) {}
	}
}
﻿using Dungeon.Functionality;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dungeon.Entities {
    public class StairEntity : IEntity {
        public int x { get; set; }
        public int y { get; set; }
        public string Name { get; set; } = "Stairs";
        public bool Solid { get; set; } = false;

        public void Action(GameState GS, bool onTop) {
            if (onTop) {

                var ME = new MessageEntity("Go down the stairs?");
                ME.Choices = new List<ChoiceEntity> {
                        new ChoiceEntity {
                            Answer = "Yes",
                            OnSelected = () => {
                                GS.Messages.Add(new MessageEntity("You go down the stairs."));
                                GS.EnterFloor();
                                ME.Answered = true;
                            },
                            Hovered = true
                        },
                        new ChoiceEntity {
                            Answer = "No",
                            OnSelected = () => {
                                ME.Answered = true;
                            }
                        }
                    
                };
                GS.Messages.Add(ME);
            } else {
                GS.Messages.Add(new MessageEntity("Yep. That's a set of stairs."));
            }
        }

        public string GoDown(GameState GS) {
            GS.Messages.Add(new MessageEntity("You go down the stairs."));
            GS.EnterFloor();
            return null;
        }
        public string NoDown(GameState GS) {
            return null;
        }

        public void Draw(GameState GS, FrameBuffer FB) {
            FB.SetPixel(x - GS.ScreenRect.x1 + 1, y - GS.ScreenRect.y1 + 1, '@');
        }

        public void Update(GameState GS) {}
    }
}

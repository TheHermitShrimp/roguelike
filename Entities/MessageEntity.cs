﻿using Dungeon.Functionality;
using System.Collections.Generic;

namespace Dungeon.Entities {

    public class MessageEntity {
        public string Message { get; set; }
        public bool _Answered { get; set; }

        public bool Answered {
            get {
                if (Choices == null || Choices.Count == 0) {
                    return true;
                }
                return _Answered;
            }
            set {
                _Answered = value;
                if (_Answered == true && Choices != null) {
                    foreach(ChoiceEntity C in Choices) {
                        C.OnSelected = null;
                    }
                }
            }
        }

        public List<ChoiceEntity> Choices { get; set; }

        public MessageEntity(string _Message) {
            this.Message = _Message;
        }
    }

    public class ChoiceEntity {
        public string Answer { get; set; }
        public bool Hovered { get; set; }
        //public MessageEntity Parent { get; set; }

        public delegate void Selected();

        public Selected OnSelected;
    }
}
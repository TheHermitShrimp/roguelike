﻿using Dungeon.Entities.Items;
using Dungeon.Entities.Items.Equipment;
using Dungeon.Functionality;
using Dungeon.Screens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dungeon.Entities {
	public class PlayerEntity : LifeForm {
		public int x;
		public int y;
		public int Dir = 3;

		public PlayerEntity() : base() {
			this.Base = new BaseStats(100, 100, 100, 100, 100);
			this.HP = this.MAX_HP;
			this.Equipment[SlotType.Weapon] = new Fist();
		}

		public bool Update(ProgramState PS, GameState GS) {
			if (HP <= 0) {
				PS.Screens.Pop();
				PS.Screens.Push(new DeathScreen());
			}
			bool ActionOccurred = false;
			if (PS.KeyInfo.HasValue) {
				if (!GS.InMessage) {
					if (PS.KeyInfo?.Key == ConsoleKey.I) {
						PS.Screens.Push(new InventoryScreen(ref GS));
					} else if (PS.KeyInfo?.Key == ConsoleKey.LeftArrow) {
						if (!GS.Map.isSolid(x - 1, y)) {
							x--;
							ActionOccurred = true;
						}
						if (Dir != 4) {
							Dir = 4;
							ActionOccurred = true;
						}

					} else if (PS.KeyInfo.Value.Key == ConsoleKey.RightArrow) {
						if (!GS.Map.isSolid(x + 1, y)) {
							x++;
							ActionOccurred = true;
						}
						if (Dir != 2) {
							Dir = 2;
							ActionOccurred = true;
						}
					} else if (PS.KeyInfo?.Key == ConsoleKey.UpArrow) {
						if (!GS.Map.isSolid(x, y - 1)) {
							y--;
							ActionOccurred = true;
						}
						if (Dir != 1) {
							Dir = 1;
							ActionOccurred = true;
						}
					} else if (PS.KeyInfo?.Key == ConsoleKey.DownArrow) {
						if (!GS.Map.isSolid(x, y + 1)) {
							y++;
							ActionOccurred = true;
						}
						if (Dir != 3) {
							Dir = 3;
							ActionOccurred = true;
						}
					} else if (PS.KeyInfo?.Key == ConsoleKey.Enter) {
						var f = GS.Map.Entities.FirstOrDefault(item =>
							(item.x == x && item.y == y) || // Underneath
							(Dir == 4 && item.x == x - 1 && item.y == y) || // Left
							(Dir == 2 && item.x == x + 1 && item.y == y) || // Right
							(Dir == 1 && item.x == x && item.y == y - 1) || // Up
							(Dir == 3 && item.x == x && item.y == y + 1) // Down
						);
						if (f != null) {
							f.Action(GS, f.x == x && f.y == y);
							//ActionOccurred = true;
						}
					} else if (PS.KeyInfo?.Key == ConsoleKey.Spacebar) {
						var f = GS.Map.Entities.Where(x => x is IEnemy).FirstOrDefault(item =>
							(Dir == 4 && item.x == x - 1 && item.y == y) || // Left
							(Dir == 2 && item.x == x + 1 && item.y == y) || // Right
							(Dir == 1 && item.x == x && item.y == y - 1) || // Up
							(Dir == 3 && item.x == x && item.y == y + 1) // Down
						);
						if (f != null && f is IEnemy && f is LifeForm) {

							int DamageDealt = BattleEngine.Attack(this, (LifeForm)f);
							GS.Messages.Add(new MessageEntity($"You attacked the {f.Name} and dealt {DamageDealt} damage."));
							if (((LifeForm)f).HP <= 0) {
								int oldLevel = this.LVL * 1;
								int NewExp = GainExp((LifeForm)f, ((IEnemy)f).KillEXP);

								GS.Messages.Add(new MessageEntity($"You gained {NewExp} experience points."));
								if (oldLevel != this.LVL) {
									GS.Messages.Add(new MessageEntity($"You have leveled up to level {this.LVL}."));
								}
								int newMoney =((IEnemy)f).MoneyDrop * ((LifeForm)f).LVL;
								if (newMoney > 0) {
									GS.Money += newMoney;
									GS.Messages.Add(new MessageEntity($"You have picked up {newMoney} gold."));
								}

							}

							ActionOccurred = true;
						}
					}
				} else {
					if (PS.KeyInfo?.Key == ConsoleKey.LeftArrow) {
						var index = GS.Messages.Last().Choices.FindIndex(x => x.Hovered);
						if (index > 0) {
							GS.Messages.Last().Choices[index].Hovered = false;
							GS.Messages.Last().Choices[index - 1].Hovered = true;
						}
					} else if (PS.KeyInfo.Value.Key == ConsoleKey.RightArrow) {
						var index = GS.Messages.Last().Choices.FindIndex(x => x.Hovered);
						if (index < GS.Messages.Last().Choices.Count - 1) {
							GS.Messages.Last().Choices[index].Hovered = false;
							GS.Messages.Last().Choices[index + 1].Hovered = true;
						}
					} else if (PS.KeyInfo?.Key == ConsoleKey.Enter) {
						GS.Messages.Last().Choices.Find(x => x.Hovered).OnSelected();
						GS.Messages.Last().Answered = true;
					}
				}
			}
			if (ActionOccurred) {
				GS.Map.Explore(this);
			}
			return ActionOccurred;
		}

		public char GetChar() {
			char c = ' ';
			switch (Dir) {
				case 1:
					c = '˄';
					break;

				case 2:
					c = '˃';
					break;

				case 3:
					c = '˅';
					break;

				case 4:
					c = '˂';
					break;
			}
			return c;
		}

		public void Draw(GameState GS, FrameBuffer FB) {
			FB.SetPixel(x - GS.ScreenRect.x1 + 1, y - GS.ScreenRect.y1 + 1, GetChar());
			FB.WriteString(34, 1, $"{GS.Map.Floor + " " + (GS.Map.FloorElement != Element.None ? GS.Map.FloorElement.Name : "")}");
			FB.WriteString(31, 3, $"{HP}/{MAX_HP}");
			FB.WriteString(31, 5, $"{MP}/{MAX_MP}");
			FB.WriteString(31, 7, $"{LVL}");
			FB.WriteString(31, 9, $"{EXP}/{GetNextExp()}");
			FB.WriteString(31, 11, $"{GS.Money}");
            FB.WriteString(50, 1, GS.Inventory.Where(x => x is DoorKey).Count().ToString());
			FB.WriteString(49, 3, $"{ATK}");
			FB.WriteString(49, 5, $"{DEF}");
			FB.WriteString(51, 7, $"{SPATK}");
			FB.WriteString(51, 9, $"{SPDEF}");
		}

		public bool isNextTo(int x, int y) {
			return (
				(this.x == x && this.y == y + 1) ||
				(this.x == x && this.y == y - 1) ||
				(this.x == x + 1 && this.y == y) ||
				(this.x == x - 1 && this.y == y)
			);
		}
	}
}

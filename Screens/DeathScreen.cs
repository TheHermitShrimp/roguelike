using System;
using Dungeon.Functionality;

namespace Dungeon.Screens {
    public class DeathScreen : IScreen {
        public void Draw(ref FrameBuffer Buffer) {
            Buffer.WriteString(1, 1, "You have died. Press enter to continue.");
        }

        public void Update(ProgramState State) {
            if (State.isKeyPressed(ConsoleKey.Enter)) {
                State.Screens.Pop();
            }
        }
    }
}
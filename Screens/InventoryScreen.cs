using System;
using System.Linq;
using System.IO;
using Dungeon.Functionality;
using Dungeon.Entities.Items.Consumable;
using System.Collections.Generic;
using Dungeon.Extensions;
using Dungeon.Entities.Items.Equipment;
using Dungeon.Entities.Items;

namespace Dungeon.Screens
{
	public class InventoryScreen : IScreen
	{
        private char[,] Gui;

        private enum Menu {
            Equipment,
            Consumables
        }

        private Menu CurrentMenu = Menu.Equipment;
        public bool MenuSelect = false;
        private GameState GS;
        private ConsumableWindow Consumables; 
        private EquipmentWindow Equipment; 

        public InventoryScreen(ref GameState _GS) {
            this.GS = _GS;
            string[] guilines = File.ReadAllLines(Directory.GetCurrentDirectory() + "/Content/inventory.txt");
            this.Gui = new char[guilines.First().Length, guilines.Count()];
            for (int y = 0; y < guilines.Count(); y++) {
                for (int x = 0; x < guilines[y].Length; x++) {
                    if (guilines[y][x] != ' ') {
                        Gui[x, y] = guilines[y][x];
                    }
                }
            }

            this.Consumables = new ConsumableWindow(ref _GS);
            this.Equipment = new EquipmentWindow(ref _GS);
        }

		public void Draw(ref FrameBuffer Buffer)
		{
			for (int y = 0; y <= Gui.GetLength(1) - 1; y++) {
                for (int x = 0; x <= Gui.GetLength(0) - 1; x++) {
                    if (Gui[x, y] != '\0') {
                        Buffer.SetPixel(x, y, Gui[x, y]);
                        Buffer.SetForeground(x, y, ConsoleColor.Gray);
                    }
                }
            }

            int SelectionY = 2;
            if (CurrentMenu == Menu.Consumables) {
                SelectionY = 4;
            }
            Buffer.SetPixel(2, SelectionY, '>');
            if (!MenuSelect) {
                Buffer.SetForeground(2, SelectionY, ConsoleColor.Green);
            }

            int messageCount = 0;
            for (int i = GS.Messages.Count() - 1; i >= 0; i--) {
                string[] MessageLines = GS.Messages[i].Message.SplitAt(77);
                foreach (string s in MessageLines) {
                    if (messageCount < 1) {
                        Buffer.WriteString(1, 18 + messageCount, s);
                        messageCount++;
                    } else {
                        break;
                    }
                }
            }

            if  (CurrentMenu == Menu.Consumables) {
                Consumables.Draw(ref Buffer, this);
            } else if (CurrentMenu == Menu.Equipment) {
                Equipment.Draw(ref Buffer, this);
            }
		}

		public void Update(ProgramState State)
		{
            if (MenuSelect == false) {
                if (State.isKeyPressed(ConsoleKey.Escape)) {
                    State.Screens.Pop();
                } else if (State.isKeyPressed(ConsoleKey.DownArrow)) {
                    if ((int)CurrentMenu < Enum.GetValues(typeof(Menu)).Cast<int>().Max()) {
                        CurrentMenu = (Menu)((int)CurrentMenu) + 1;
                    }
                } else if (State.isKeyPressed(ConsoleKey.UpArrow)) {
                    if ((int)CurrentMenu > Enum.GetValues(typeof(Menu)).Cast<int>().Min()) {
                        CurrentMenu = (Menu)((int)CurrentMenu) - 1;
                    }
                } else if (State.isKeyPressed(ConsoleKey.Enter)) {
                    MenuSelect = true;
                }
            } else if (MenuSelect) {
                if (CurrentMenu == Menu.Consumables) {
                    Consumables.Update(State, this);
                } else if (CurrentMenu == Menu.Equipment) {
                    Equipment.Update(State, this);
                }
            }
		}

        private class ConsumableWindow {
            GameState GS;
            
            int MenuIndex = 0;
            int MenuSelectedIndex = -1;
            public bool SubMenuSelected = false;

            int SelectedOption = 0;
            string[] Options = new string[] {
                "Use",
                "Drop",
                "Trash"
            };

            private List<IConsumable> Consumables {
                get {
                    return GS.Inventory.Where(x => x.Type == Entities.Items.ItemType.Consumable).Select(x => (IConsumable)x).ToList();
                }
            }

            public ConsumableWindow(ref GameState _GS) {
                GS = _GS;
                if (Consumables.Count > 0) {
                    MenuSelectedIndex = 0;
                }
            }

            public void Update(ProgramState State, InventoryScreen Screen) {
                
                if (SubMenuSelected == false) {
                    if (State.isKeyPressed(ConsoleKey.Escape)) {
                        Screen.MenuSelect = false;
                    } else if (State.isKeyPressed(ConsoleKey.DownArrow)) {
                        if (MenuSelectedIndex < GS.Inventory.Count - 1) {
                            MenuSelectedIndex++;
                            if (MenuSelectedIndex >= (MenuIndex + 13)) {
                                MenuIndex++;
                            }
                        }
                    } else if (State.isKeyPressed(ConsoleKey.UpArrow)) {
                        if (MenuSelectedIndex > 0) {
                            MenuSelectedIndex --;
                            if (MenuSelectedIndex < MenuIndex) {
                                MenuIndex = MenuSelectedIndex;
                            }
                        }
                    } else if (State.isKeyPressed(ConsoleKey.Enter)) {
                        if (MenuSelectedIndex > -1) {
                            SubMenuSelected = true;
                        }
                    }
                } else if (SubMenuSelected == true) {
                    if (State.isKeyPressed(ConsoleKey.Escape)) {
                        SubMenuSelected = false;
                    } else if (State.isKeyPressed(ConsoleKey.LeftArrow)) {
                        if (SelectedOption > 0) {
                            SelectedOption--;
                        }
                    } else if (State.isKeyPressed(ConsoleKey.RightArrow)) {
                        if (SelectedOption < Options.Count() - 1) {
                            SelectedOption++;
                        }
                    } else if (State.isKeyPressed(ConsoleKey.Enter)) {
                        if (Options[SelectedOption] == "Use") {
                            if (Consumables[MenuSelectedIndex].Use(GS)) {
                                GS.Inventory.Remove(Consumables[MenuSelectedIndex]);
                                MoveIndex();
                            }
                        } else if (Options[SelectedOption] == "Drop") {
                            IItems I = Consumables[MenuSelectedIndex];
                            GS.Inventory.Remove(I);
                            GS.Map.Entities.Add(new ItemDrop(I, new Point(GS.Player.x, GS.Player.y)));
                            MoveIndex();
                        } else if (Options[SelectedOption] == "Trash") {
                            IItems I = Consumables[MenuSelectedIndex];
                            GS.Inventory.Remove(I);
                            MoveIndex();
                        }
                    }
                }
            }

            private void MoveIndex() {
                if (MenuSelectedIndex > 0 && Consumables.Count > 0) {
                    MenuSelectedIndex--;
                } else if (Consumables.Count == 0) {
                    MenuSelectedIndex = -1;
                }
                SubMenuSelected = false;
            }

            public void Draw(ref FrameBuffer Buffer, InventoryScreen Screen) {
                int j = 0;
                for (int i = MenuIndex; i <= Math.Min(Consumables.Count - 1, MenuIndex + 12); i++) {
                    Buffer.WriteString(24, j + 1, Consumables[i].Name);
                    j++;
                }
                if (MenuSelectedIndex > -1 && Consumables.Count > 0) {
                    Buffer.SetPixel(22, MenuSelectedIndex - MenuIndex + 1, '>');
                    if (Screen.MenuSelect && SubMenuSelected == false) {
                        Buffer.SetForeground(22, MenuSelectedIndex - MenuIndex + 1, ConsoleColor.Green);
                    }
                    Buffer.WriteString(22, 15, Consumables[MenuSelectedIndex].Description);
                }

                if (SubMenuSelected == true) {
                    int stringLength = 22;
                    for(int i = 0; i < Options.Length; i++) {
                        if(SelectedOption == i) {
                            Buffer.SetPixel(stringLength, 16, '>');
                            Buffer.SetForeground(stringLength, 16, ConsoleColor.Green);
                        }
                        stringLength += 2;
                        Buffer.WriteString(stringLength, 16, Options[i]);
                        stringLength += Options[i].Length + 2;
                    }
                }
            }
        }

        private class EquipmentWindow {
            private GameState GS;

            private List<IEquipment> CurrentEquipment = new List<IEquipment>();
            private int menuState = 1;
            private SlotType[] SlotMap = new SlotType[] {
                SlotType.Weapon,
                SlotType.Armor,
                SlotType.Helmet,
                SlotType.Boots,
                SlotType.Etc1,
                SlotType.Etc2
            };
            private SlotType CurrentSlot = SlotType.Weapon;

            private int submenuindex = -1;
            private int submoverindex = -1;
            

            private List<IEquipment> Equipment {
                get {
                    return GS.Inventory.Where(x => x.Type == Entities.Items.ItemType.Equipment).Select(x => (IEquipment)x).ToList();
                }
            }

            public EquipmentWindow(ref GameState _GS) {
                GS = _GS;
            }
            public void Update(ProgramState State, InventoryScreen Screen) {
                if (menuState == 1) {
                    if (State.isKeyPressed(ConsoleKey.Escape)) {
                        Screen.MenuSelect = false;
                    } else if (State.isKeyPressed(ConsoleKey.UpArrow)) {
                        if (CurrentSlot - 2 >= 0) {
                            CurrentSlot = SlotMap[(int)CurrentSlot - 2];
                        }
                    } else if (State.isKeyPressed(ConsoleKey.DownArrow)) {
                        if ((int)CurrentSlot + 2 <= SlotMap.Length - 1) {
                            CurrentSlot = SlotMap[(int)CurrentSlot + 2];
                        }
                    } else if (State.isKeyPressed(ConsoleKey.RightArrow)) {
                        if ((int)CurrentSlot % 2 == 0) {
                            CurrentSlot = SlotMap[(int)CurrentSlot + 1];
                        }
                    } else if (State.isKeyPressed(ConsoleKey.LeftArrow)) {
                        if ((int)CurrentSlot % 2 != 0) {
                            CurrentSlot = SlotMap[(int)CurrentSlot - 1];
                        }
                    } else if (State.isKeyPressed(ConsoleKey.Enter)) {
                        menuState = 2;
                        UpdateCurrentEquipment();
                        if (CurrentEquipment.Count > 0) {
                            submenuindex = 0;
                        } else {
                            submenuindex = -1;
                        }
                        submoverindex = submenuindex;
                    }
                } else if (menuState == 2) {
                    if (State.isKeyPressed(ConsoleKey.Escape)) {
                        menuState = 1;
                    } else if (State.isKeyPressed(ConsoleKey.DownArrow)) {
                        if (submoverindex < CurrentEquipment.Count - 1) {
                            submoverindex ++;
                            if (submoverindex >= submenuindex + 13) {
                                submenuindex++;
                            }
                        }
                    } else if (State.isKeyPressed(ConsoleKey.UpArrow)) {
                        if (submoverindex > 0) {
                            submoverindex --;
                            if (submoverindex < submenuindex) {
                                submenuindex--;
                            }
                        }
                    } else if (State.isKeyPressed(ConsoleKey.Enter)) {
                        if (submoverindex > -1) {
                            IEquipment E = GS.Player.Equipment[CurrentSlot];
                            IEquipment E2 = CurrentEquipment[submoverindex];
                            GS.Inventory.Remove(E2);
                            if (E != null) {
                                GS.Inventory.Add(E);
                            }
                            GS.Player.Equipment[CurrentSlot] = E2;
                            UpdateCurrentEquipment();
                            if (submenuindex >= CurrentEquipment.Count) {
                                submenuindex = CurrentEquipment.Count - 1;
                            }
                            menuState = 1;
                        }
                    }
                }
            }

            private void UpdateCurrentEquipment() {
                CurrentEquipment = Equipment.Where(x => x.Slots.Contains(CurrentSlot)).ToList();
            }

            public void Draw(ref FrameBuffer Buffer, InventoryScreen Screen) {
                IEquipment CurrentItem = null;
                if (menuState == 1) {
                    string defaultText = "------";
                    int pointerx = 0, pointery = 0;
                    if (CurrentSlot == SlotType.Weapon) { pointerx = 23; pointery = 3; CurrentItem = GS.Player.Equipment[SlotType.Weapon]; }
                    Buffer.WriteString(25, 2, "Weapon");
                    Buffer.WriteString(25, 3, GS.Player.Equipment[SlotType.Weapon]?.Name ?? defaultText);

                    if (CurrentSlot == SlotType.Armor) { pointerx = 49; pointery = 3; CurrentItem = GS.Player.Equipment[SlotType.Armor]; }
                    Buffer.WriteString(51, 2, "Armor");
                    Buffer.WriteString(51, 3, GS.Player.Equipment[SlotType.Armor]?.Name ?? defaultText);

                    if (CurrentSlot == SlotType.Helmet) { pointerx = 23; pointery = 6; CurrentItem = GS.Player.Equipment[SlotType.Helmet]; }
                    Buffer.WriteString(25, 5, "Helmet");
                    Buffer.WriteString(25, 6, GS.Player.Equipment[SlotType.Helmet]?.Name ?? defaultText);

                    if (CurrentSlot == SlotType.Boots) { pointerx = 49; pointery = 6; CurrentItem = GS.Player.Equipment[SlotType.Boots]; }
                    Buffer.WriteString(51, 5, "Boots");
                    Buffer.WriteString(51, 6, GS.Player.Equipment[SlotType.Boots]?.Name ?? defaultText);

                    if (CurrentSlot == SlotType.Etc1) { pointerx = 23; pointery = 9; CurrentItem = GS.Player.Equipment[SlotType.Etc1]; }
                    Buffer.WriteString(25, 8, "Etc. 1");
                    Buffer.WriteString(25, 9, GS.Player.Equipment[SlotType.Etc1]?.Name ?? defaultText);

                    if (CurrentSlot == SlotType.Etc2) { pointerx = 49; pointery = 9; CurrentItem = GS.Player.Equipment[SlotType.Etc2]; }
                    Buffer.WriteString(51, 8, "Etc 2.");
                    Buffer.WriteString(51, 9, GS.Player.Equipment[SlotType.Etc2]?.Name ?? defaultText);

                    Buffer.SetPixel(pointerx, pointery, '>');
                    if (Screen.MenuSelect) {
                        Buffer.SetForeground(pointerx, pointery, ConsoleColor.Green);
                    }
                } else {
                    
                    for (int i = Math.Max(submenuindex, 0), j = 0; i < Math.Min(CurrentEquipment.Count, submenuindex + 13); i++, j++) {
                        if (i == submoverindex) {Buffer.SetPixel(23, 1 + j, '>'); Buffer.SetForeground(23, 1 + j, ConsoleColor.Green); CurrentItem=CurrentEquipment[i]; }
                        Buffer.WriteString(25, 1 + j, CurrentEquipment[i].Name);
                    }
                }

                if (CurrentItem != null && Screen.MenuSelect) {
                    Buffer.WriteString(22, 15, CurrentItem.Description);
                    List<string> Stats = new List<string>();
                    if (CurrentItem.HP > 0) Stats.Add("HP: " + CurrentItem.HP);
                    if (CurrentItem.ATK > 0) Stats.Add("ATK: " + CurrentItem.ATK);
                    if (CurrentItem.DEF > 0) Stats.Add("DEF: " + CurrentItem.DEF);
                    if (CurrentItem.SPATK > 0) Stats.Add("SPATK: " + CurrentItem.SPATK);
                    if (CurrentItem.SPDEF > 0) Stats.Add("SPDEF: " + CurrentItem.SPDEF);

                    Buffer.WriteString(22, 16, String.Join(" | ", Stats));
                }
            }
        }
	}
}
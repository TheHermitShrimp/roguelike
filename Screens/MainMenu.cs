﻿using Dungeon.Functionality;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dungeon.Screens {
	public class MainMenu : IScreen {

		private bool OffScreen = false; 
		private bool HasSaveState = false;

		private int menuindex = 2;
		private Dictionary<int, string> Menus = new Dictionary<int, string>() {
			{1, "Load Game"},
			{2, "New Game"}
			
		};

		public MainMenu() {
			HasSaveState = GameScreen.LoadSave() != null;
			if (HasSaveState) menuindex = 1;
		}

		public void Draw(ref FrameBuffer Buffer) {

			int incrementer = 0;
			foreach(var Menu in Menus) {
				if (Menu.Key != 1 || (Menu.Key == 2 && HasSaveState) ) {
					if (Menu.Key == menuindex) {
						Buffer.SetPixel(1, incrementer * 2, '>');
						Buffer.SetForeground(3, incrementer * 2, ConsoleColor.Green);
					}
					Buffer.WriteString(3, incrementer * 2, Menu.Value);
				}
				incrementer ++;
			}
		}

		public void Update(ProgramState State) {
			if (OffScreen) {
				HasSaveState = GameScreen.LoadSave() != null;
				OffScreen = false;
			}
			if (State.isKeyPressed(ConsoleKey.Enter)) {
				if (menuindex == 2) {
					State.Screens.Push(new GameScreen());
					OffScreen = true;
				} else if (menuindex == 1) {
					State.Screens.Push(new GameScreen(GameScreen.LoadSave()));
					OffScreen = true;
				}
				
			} else if (State.isKeyPressed(ConsoleKey.DownArrow)) {
				if (menuindex < Menus.Keys.Max()) {
					menuindex++;
				}
			} else if (State.isKeyPressed(ConsoleKey.UpArrow)) {
				if (menuindex > Menus.Keys.Min()) {
					menuindex--;
				}
			}
		}
	}
}

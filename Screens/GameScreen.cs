﻿using Dungeon.Entities;
using Dungeon.Entities.Items.Consumable;
using Dungeon.Entities.Items.Equipment;
using Dungeon.Extensions;
using Dungeon.Functionality;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;

namespace Dungeon.Screens {

    public class GameScreen : IScreen {
        private GameState GS = null;
        private Random R = new Random();

        private char[,] Gui;

        public GameScreen(GameState GS = null) {
            string[] guilines = File.ReadAllLines(Directory.GetCurrentDirectory() + "/Content/Gui.txt");
            this.Gui = new char[guilines.First().Length, guilines.Count()];
            for (int y = 0; y < guilines.Count(); y++) {
                for (int x = 0; x < guilines[y].Length; x++) {
                    if (guilines[y][x] != ' ') {
                        Gui[x, y] = guilines[y][x];
                    }
                }
            }
            if (GS == null) {
                this.GS = new GameState();
                this.GS.EnterFloor();
            } else {
                this.GS = GS;
                this.GS.Map.GenerateTileTypes();
            }
        }

        public void Draw(ref FrameBuffer Buffer) {
            int truey = 1;

            for (int y = 0; y <= Gui.GetLength(1) - 1; y++) {
                for (int x = 0; x <= Gui.GetLength(0) - 1; x++) {
                    if (Gui[x, y] != '\0') {
                        Buffer.SetPixel(x, y, Gui[x, y]);
                        Buffer.SetForeground(x, y, ConsoleColor.Gray);
                    }
                }
            }

            for (int y = GS.ScreenRect.y1; y < GS.ScreenRect.y2; y++) {
                int truex = 1;
                for (int x = GS.ScreenRect.x1; x < GS.ScreenRect.x2; x++) {
                    bool HasVision = false;
                    char c = ' ';
                    ConsoleColor FG = ConsoleColor.Gray;
                    ConsoleColor BG = ConsoleColor.Black;
                    if (y < 0 || x < 0 || y > GS.Map.MapData.GetLength(1) - 1 || x > GS.Map.MapData.GetLength(0) - 1) {
                    } else if (GS.Map.Explored[x, y] == 1) {
                        if (GS.Map.Vision.Contains(new Point(x, y))) {
                            FG = ConsoleColor.White;
                            HasVision = true;
                        }
                        var T = GS.Map.Tiles.Find(t => t.ID == GS.Map.MapData[x, y]);
                        c = T.C;
                        if (T.Color.FG.HasValue) {
                            FG = T.Color.FG.Value;
                        }
                        if (GS.Map.MapData[x,y] == 0 && GS.Map.FloorElement != Element.None) {
                            BG = GS.Map.FloorElement.Color;
                        } else if (T.Color.BG.HasValue) {
                            BG = T.Color.BG.Value;
                            
                        }
                        if (HasVision && T.Color.Vision != null) {
                            if (T.Color.Vision.FG.HasValue) {
                                FG = T.Color.Vision.FG.Value;
                            }
                            if (T.Color.Vision.BG.HasValue) {
                                BG = T.Color.Vision.BG.Value;
                            }
                        }
                    }
                    Buffer.SetPixel(truex, truey, c);
                    Buffer.SetForeground(truex, truey, FG);
                    Buffer.SetBackground(truex, truey, BG);

                    truex++;
                }
                truey++;
            }

            foreach (IEntity E in GS.Map.Entities) {
                if (GS.ScreenRect.isInside(E.x, E.y) && (
                        (GS.Map.Vision.Contains(new Point(E.x, E.y)) && E is LifeForm) ||
                        (GS.Map.Explored[E.x, E.y] == 1 && E is not LifeForm)
                    )) {
                    E.Draw(GS, Buffer);
                }
            }

            int messageCount = 0;
            int screenHeight = 14;
            for (int i = GS.Messages.Count() - 1; i >= 0; i--) {
                if (GS.Messages[i].Choices?.Count > 0 && messageCount < 5) {
                    Buffer.WriteString(1, screenHeight + messageCount, GS.Messages[i].Message);
                    int writelength = 1 + GS.Messages[i].Message.Length + 1;
                    foreach (ChoiceEntity CE in GS.Messages[i].Choices) {
                        Buffer.WriteString(writelength, screenHeight + messageCount, CE.Answer, ConsoleColor.White, CE.Hovered ? ConsoleColor.DarkGreen : ConsoleColor.Black);
                        writelength += CE.Answer.Length + 1;
                    }
                    messageCount++;
                } else {
                    string[] MessageLines = GS.Messages[i].Message.SplitAt(77);
                    foreach (string s in MessageLines) {
                        if (messageCount < 5) {
                            Buffer.WriteString(1, screenHeight + messageCount, s);
                            messageCount++;
                        } else {
                            break;
                        }
                    }

                }
            }

            GS.Player.Draw(GS, Buffer);
        }

        public void Update(ProgramState State) {
            if (State.isKeyPressed(ConsoleKey.Escape)) {
                State.Screens.Pop();
            }

            if (GS.Player.Update(State, GS)) {
                for (int i = GS.Map.Entities.Count - 1; i >= 0; i--) {
                    GS.Map.Entities[i].Update(GS);
                }
                AutoSave();
            }
            GS.UpdateScreenRect();
            
        }
        private string LastSave = null;
        private static string SaveFile = Directory.GetCurrentDirectory() + "/Content/AutoSave.json";
        private static JsonSerializerSettings jsonSettings = new JsonSerializerSettings {
                NullValueHandling = NullValueHandling.Ignore,
                TypeNameHandling = TypeNameHandling.Auto
        };
        private void AutoSave() {
            string SaveData = JsonConvert.SerializeObject(GS, Formatting.None, jsonSettings);
            if (SaveData != LastSave) {
                File.WriteAllText(SaveFile, SaveData);
                LastSave = SaveData;
            }
        }

        public static GameState LoadSave() {
            string SaveData = File.ReadAllText(SaveFile);
            GameState GS = JsonConvert.DeserializeObject<GameState>(SaveData, jsonSettings);
            if (GS?.Map?.Floor > 0) {
                return GS;
            }
            return null;
        }
    }
}
﻿using Dungeon.Functionality;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dungeon.Screens {
    public interface IScreen {
        void Update(ProgramState State);
        void Draw(ref FrameBuffer Buffer);
    }
}

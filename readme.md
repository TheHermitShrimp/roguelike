# A standard roguelike game

Not much to say. It is what it is. Just building it out of interest.

## Compiling
Nothing fancy. Runs .NET 5.0. I use VS Code for development.

## Features so far
- Random dungeon map generation
  - With themes, monster spawns, item drops
- Monsters with distinct AI (Yes, only 2 so far)
- Monster Encyclopedia to make adding new monsters easy
- Battle system with stats, equipment, elements, etc.
- Usable items (so far just potions)
- Equipment that affects stats
- Inventory menu
- Locked doors, treasure rooms, and keys

## Features coming whenever
- Treasure Boxes
- Better Main screen
  - Saving/Loading
- Building out the monster encyclopedia
- Building out the item encyclopedia
- NPCs
  - Quests of some sort
- Money
  - Shopkeepers?
- Bosses
- Projectiles
- Interactions with attack types
  - for instance slashing being more effective than stabbing or something

Feel free to submit changes, report issues, make suggestions.